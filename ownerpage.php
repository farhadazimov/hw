<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ownerpage</title>
  <a href='homepage.php'>Homepage</a>
  <link rel="stylesheet" href="ownerpage.css">
</head>
<body>
  
<?php 
session_start();
echo $_SESSION["owner"];
if(!isset($_SESSION["owner"])){
  header('Location: homepage.php');
}
else if($_SESSION["owner"]==0)
{
  header('Location: homepage.php');

}
else {
include("config.php");
$pid=$_SESSION["pid"];
$stmt=$pdo->query("SELECT users.firstname,users.lastname,projects_investors.investmentFund 
FROM projects_investors INNER JOIN users ON users.idUser = projects_investors.idUser WHERE projects_investors.idProject=$pid;");
$stmt->execute();
$name=$_SESSION['name'];
echo "<h1>Project: $name</h1>";
echo ' <table style="width:90%">
  <tr style="font-weight: bold;">
<td>Investor</td>
<td>Invested fund</td>
</tr>';
$total=0;
$needed=$_SESSION["fund"];
while($result=$stmt->fetch())
{
  $name=$result['firstname']." ".$result['lastname'];
  $fund=$result['investmentFund'];
  $total+=$fund;
  echo "<tr>
  <td>$name</td>
  <td>$fund</td>
  </tr>";
}
$expected=$needed-$total;
echo "<p> Total raised: $total Expected: $expected </p>";
}
echo "</table></body></html>";
?>