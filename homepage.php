<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Crowdfunding</title>
  <link rel="stylesheet" href="homepage.css">
</head>
<body>
<!-- <a href="login.html">Login</a> -->

<?php
session_start();
include("config.php");
echo "<nav>";
$ID=$_SESSION['user'];
if($ID!=-1){
  echo '<a href="logout.php">Logout</a>';
  if($_SESSION["owner"]==1)
  echo '<a href="ownerpage.php">Owner page</a>';
  echo '<a href="invest.html">Invest page</a>';
}
else{
  echo '<a href="login.html">Login</a>';
}
echo "</nav>";


$stmt=$pdo->query("SELECT users.firstname,users.lastname,projects.projectName,
projects.projectDescription,projects.projectEndDate,projects.requestedFund
FROM users INNER JOIN projects ON users.idUser = projects.idUser ");
$stmt->execute();


echo ' <h1>Projects</h1>
<table style="width:90%">
<tr style="font-weight: bold;">
<td>Owner</td>
<td>Title</td>
<td>Description</td>
<td>End date</td>
<td>Requested fund</td>
</tr>';
while($result=$stmt->fetch())
{
  $name=$result['firstname']." ".$result['lastname'];
  $date=$result['projectEndDate'];
  $fund=$result['requestedFund'];
  $title=$result['projectName'];
  $description=$result['projectDescription'];
  echo "<tr>
  <td>$name</td>
  <td>$title</td>
  <td>$description</td>
  <td>$date</td>
  <td>$fund</td>
  </tr>";
}
echo "</table></body></html>";


?>
