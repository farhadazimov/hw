<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Error1</title>
    <link rel="stylesheet" href="error.css">
</head>
<body>

<?php
session_start();
echo '<a href="invest.html">Back</a>';
if($_SESSION["error"]==1){
echo "<h1>There is no such project in the database!</h1>";
}
else if($_SESSION["error"]==3)
{
echo "<h1>Owner cannot contribute to his own projects!</h1>";
}
else if($_SESSION["error"]==4)
{
 echo "<h1>You have already contributed to this project!</h1>";
}
else if($_SESSION["error"]==5)
{
 echo "<h1>Contribution is bigger than expected value!</h1>";
}
else if($_SESSION["error"]==2)
{
 echo "<h1>Due date is later than ending date!</h1>";
}

session_destroy();

echo "</body></html>";
?>